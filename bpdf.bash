#!/bin/bash


if [ $# == 0 ]
then
  echo "To merge in1.pdf .. inn.pdf to a single file out.pdf:"
  echo "bpdf merge out.pdf in1.pdf .. inn.pdf"
  echo ""
  echo "To remove the first page of each of file1.pdf .. filen.pdf:"
  echo "bpdf rmfp file1.pdf .. filen.pdf"
  exit
fi


if [ "$1" = "merge" ]
then
  if [ $# -le 3 ]
  then
    echo "Merging requires at least three parameters."
  else
    if [ -e $2 ]
    then
      echo "I'm not going to write over an existing file."
    else
      o="$2"
      x=""
      shift
      shift
      while [ $# -gt 0 ]
      do
        if [ -e $1 ]
        then
          x="$x $1"
	  shift
        else
	  echo $1 is not a file.
	  exit
	fi
      done
      gs -dAutoRotatePages=/None \
         -dNOPAUSE -sDEVICE=pdfwrite \
         -sOUTPUTFILE=$o \
         -dBATCH $x
    fi
  fi
fi



if [ "$1" = "rmfp" ]
then
  if [ $# -lt 2 ]
  then
    echo "We will need at least one parameter."
    exit
  else
    shift
    while [ $# -ge 1 ]
    do
      x="$1"
      if [ -e $1 ]
      then
        echo "Removing first page of $x."
        mv $x ${x}.old
        gs -dAutoRotatePages=/None \
           -dBATCH \
           -dNOPAUSE \
           -sOutputFile=$x \
           -dFirstPage=2 \
           -sDEVICE=pdfwrite \
           ${x}.old
      else
        echo $1 is not a file.
      fi
      shift
    done
  fi
fi


if [ "$1" = "split" ]
then
  if [ $# -lt 2 ]
  then
    echo "Splitting requires one parameter."
  else
    gs -dAutoRotatePages=/None \
       -dBATCH \
       -dNOPAUSE \
       -sOutputFile=output-%03d.pdf \
       -sDEVICE=pdfwrite \
       $2
  fi
fi
