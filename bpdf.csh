#!/bin/csh

if ( $#argv == 0 ) then
  echo "To merge in1.pdf .. inn.pdf to a single file out.pdf:"
  echo "bpdf merge out.pdf in1.pdf .. inn.pdf"
  echo ""
  echo "To remove the first page of each of file1.pdf .. filen.pdf:"
  echo "bpdf rmfp file1.pdf .. filen.pdf"
  exit
endif


if $argv[1] == merge then
  if ( $#argv <= 3 ) then
    echo "Merging requires at least three parameters."
  else
    if -e $argv[2] then
      echo "I'm not going to write over an existing file."
    else
      set o = $argv[2]
      set x = ""
      shift
      shift
      while ( $#argv >= 1 )
        if ( -e $argv[1] ) then
          set x = "$x $argv[1]"
	  shift
        else
	  echo $argv[1] is not a file.
	  exit
	endif
      end
      gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=$o -dBATCH $x
    endif
  endif
endif



if $argv[1] == rmfp then
  if ( $#argv < 2 ) then
    echo "We will need at least one parameter."
    exit
  else
    shift
    while ( $#argv >= 1 )
      set x = $argv[1]
      if -e $argv[1] then
        echo Removing first page of $x.
        mv $x ${x}.old
	gs -dBATCH -dNOPAUSE -sOutputFile=$x -dFirstPage=2 -sDEVICE=pdfwrite ${x}.old
      else
        echo $argv[1] is not a file.
      endif
      shift
    end
  endif
endif
